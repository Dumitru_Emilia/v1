function textProcessor(input, tokens) {
    if (typeof input != "string") {
        throw new Error("Input should be a string");
    }
    if (input.length < 6) {
        throw new Error("Input should have at least 6 characters")
    }

    var i = 0;
    for (; i < tokens.length; i++) {
        if (typeof tokens[i].tokenName != "string" || typeof tokens[i].tokenValue != "string") {
            throw new Error("Invalid array format");
        }
    }
    let result = input;
    tokens.forEach(el => {
        if (input.includes("${" + el.tokenName + "}")) {
            result = result.replace("${" + el.tokenName + "}", el.tokenValue)
        }
    })
    return result;
}



const app = {
    textProcessor: textProcessor
};

module.exports = app;